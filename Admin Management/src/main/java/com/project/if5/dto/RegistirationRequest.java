package com.project.if5.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class RegistirationRequest {

	private String username;

	private String password;

	private String firstname;

	private String lastname;

	@Email
	private String email;
}
