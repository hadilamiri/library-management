package com.project.if5.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Data
public class UserDto {

	private String username;

	private String firstname;

	private String lastname;

	@Email
	private String email;

}
