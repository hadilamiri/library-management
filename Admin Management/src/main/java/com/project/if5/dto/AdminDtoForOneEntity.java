package com.project.if5.dto;

import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Data
@NoArgsConstructor
public class AdminDtoForOneEntity {
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private String surname;

    @Email
    private String email;
}
