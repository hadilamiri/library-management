package com.project.if5.repository;

import com.project.if5.model.Admin;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface AdminRepository  extends JpaRepository<Admin, Long> {

    Admin findByEmail(String email);

    @Query("select a from Admin a where a.name like %:name% or a.surname like %:surname%")
    List<Admin> findByNameOrSurname(String name, String surname);

}
