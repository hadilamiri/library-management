package com.project.if5.repository;


import com.project.if5.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, Long> {

	List<User> findByEmail(String email);

	@Query("select u from User u where u.username like %:username%")
	List<User> getByUsername(@Param("username") String username);

	User findByUsername(String username);

}
