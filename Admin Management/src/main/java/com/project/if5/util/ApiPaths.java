package com.project.if5.util;

public final class ApiPaths {

	private static final String BASE_PATH = "/api";

	public static final class MainCtrl {
		public static final String CTRL = BASE_PATH + "/main";
	}
	public static final class AdminCtrl {
		public static final String CTRL = BASE_PATH + "/admin";
	}

	public static final class UserCtrl {
		public static final String CTRL = BASE_PATH + "/user";
	}

}
