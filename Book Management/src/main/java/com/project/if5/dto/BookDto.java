package com.project.if5.dto;
import com.project.if5.model.BookStatus;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Data
public class BookDto {

	private Long id;

	@NotNull
	private String name;

	@NotNull
	private String barcode;

	private String content;

	@NotNull
	private String publisher;

	private BookStatus bookStatus;


}
