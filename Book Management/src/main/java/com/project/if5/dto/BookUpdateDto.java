package com.project.if5.dto;
import com.project.if5.model.BookStatus;
import com.sun.istack.NotNull;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@Data
public class BookUpdateDto {

	private Long id;
	@NotNull
	private String name;

	private String content;

	@NotNull
	private Long authorId;

	@NotNull
	private String barcode;

	@NotNull
	private BookStatus bookStatus;

	@NotNull
	private String publisher;
}
