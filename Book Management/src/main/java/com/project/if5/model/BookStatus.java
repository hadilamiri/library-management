package com.project.if5.model;

public enum BookStatus {
	  USED,
	  FREE,
	  NOTPUBLISHED,
	  PUBLISHED
}
