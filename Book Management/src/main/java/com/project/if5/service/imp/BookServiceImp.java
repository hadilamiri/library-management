package com.project.if5.service.imp;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.project.if5.model.Book;
import com.project.if5.service.BookService;
import com.project.if5.repository.BookRepository;
import com.project.if5.dto.*;
import com.project.if5.util.TPage;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import javassist.NotFoundException;

@Service
public class BookServiceImp implements BookService {
	private final ModelMapper modelMapper;
	private final BookRepository bookRepository;

	public BookServiceImp(ModelMapper modelMapper,BookRepository bookRepository) {
		super();
		this.modelMapper = modelMapper;
		this.bookRepository = bookRepository;
	}

	public BookOneDto save(BookOneDto bookOneDto) throws Exception {

		List<Book> bookChecked = bookRepository.findByName(bookOneDto.getName().trim());
		if (bookChecked.size() > 0) {
			throw new Exception("book already exist");
		}

		Book book = modelMapper.map(bookOneDto, Book.class);
		bookRepository.save(book);

		bookOneDto.setId(book.getId());
		return bookOneDto;
	}

	public List<BookDto> getAll() throws NotFoundException {

		List<Book> books = bookRepository.findAll(Sort.by(Sort.Direction.ASC, "id"));
		if (books.size() < 1) {
			throw new NotFoundException("Book don't already exist");
		}
		BookDto[] bookDtos = modelMapper.map(books, BookDto[].class);
		return Arrays.asList(bookDtos);

	}

	public TPage<BookDto> getAllPageable(Pageable pageable) throws NotFoundException {

		try {
			Page<Book> page = bookRepository.findAll(PageRequest.of(pageable.getPageNumber(), pageable.getPageSize(),
					Sort.by(Sort.Direction.ASC, "id")));
			TPage<BookDto> tPage = new TPage<BookDto>();
			BookDto[] bookDtos = modelMapper.map(page.getContent(), BookDto[].class);
			tPage.setStat(page, Arrays.asList(bookDtos));
			return tPage;
		} catch (Exception e) {
			throw new NotFoundException("Book does not exist : ");
		}
	}

	@Transactional
	public BookUpdateDto update(Long id, BookUpdateDto bookUpdateDto) throws NotFoundException {

		Optional<Book> bookOpt = bookRepository.findById(id);
		if (!bookOpt.isPresent()) {
			throw new NotFoundException("Book does not exist id : " + id);
		}
		Book realbook = modelMapper.map(bookUpdateDto, Book.class);
		bookRepository.save(realbook);
		bookUpdateDto = modelMapper.map(realbook, BookUpdateDto.class);
		return bookUpdateDto;

	}

	public BookOneDto getOne(Long id) throws NotFoundException {

		Optional<Book> book = bookRepository.findById(id);
		if (!book.isPresent()) {
			throw new NotFoundException("Book does not exist id : " + id);
		}
		BookOneDto bookOneDto = modelMapper.map(book.get(), BookOneDto.class);
		bookOneDto.setId(id);


		return bookOneDto;

	}

	public Boolean delete(Long id) throws NotFoundException {

		Optional<Book> book = bookRepository.findById(id);
		if (!book.isPresent()) {
			throw new NotFoundException("Book does not exist id : " + id);
		}
		bookRepository.deleteById(id);
		return true;

	}

	public List<BookDto> SearchBooksByName(String name) throws NotFoundException {
		List<Book> books = bookRepository.SearchBooksByName(name.trim());
		if (books.size() < 1) {
			throw new NotFoundException("Book don't already exist");
		}
		BookDto[] bookDtos = modelMapper.map(books, BookDto[].class);
		return Arrays.asList(bookDtos);
	}

}
