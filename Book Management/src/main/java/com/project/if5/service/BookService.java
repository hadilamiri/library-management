package com.project.if5.service;

import java.util.List;

import com.project.if5.dto.*;
import com.project.if5.util.TPage;
import javassist.NotFoundException;
import org.springframework.data.domain.Pageable;



public interface BookService {
	public BookOneDto save(BookOneDto bookOneDto) throws Exception;
	public List<BookDto> getAll() throws NotFoundException;
	public TPage<BookDto> getAllPageable(Pageable pageable) throws NotFoundException;
	public BookUpdateDto update(Long id, BookUpdateDto bookUpdateDto) throws NotFoundException;
	public BookOneDto getOne(Long id) throws NotFoundException ;
	public Boolean delete(Long id) throws NotFoundException;
	public List<BookDto> SearchBooksByName(String name) throws NotFoundException;
}
