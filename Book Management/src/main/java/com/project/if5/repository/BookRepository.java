package com.project.if5.repository;
import com.project.if5.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import java.util.List;


public interface BookRepository extends JpaRepository<Book, Long> {

	List<Book> findByName(String name);

	@Query("select b from Book b where b.name like %:name%")
	List<Book> SearchBooksByName(String name);

}
