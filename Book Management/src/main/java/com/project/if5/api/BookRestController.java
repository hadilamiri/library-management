package com.project.if5.api;

import java.util.Arrays;
import java.util.List;
import com.project.if5.dto.BookDto;
import com.project.if5.dto.BookOneDto;
import com.project.if5.dto.BookUpdateDto;
import com.project.if5.model.BookStatus;
import com.project.if5.service.imp.BookServiceImp;
import com.project.if5.util.TPage;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javassist.NotFoundException;

@RestController
@RequestMapping("/api/book")
@CrossOrigin
public class BookRestController {

	private final BookServiceImp bookServiceImp;

	public BookRestController(BookServiceImp bookServiceImp) {
		super();
		this.bookServiceImp = bookServiceImp;
	}

	@GetMapping()
	public ResponseEntity<List<BookDto>> getAll() throws NotFoundException {
		return ResponseEntity.ok(bookServiceImp.getAll());
	}

	// localhost:8182/api/book/pagination?page=1&size=3
	@GetMapping("/pagination")
	public ResponseEntity<TPage<BookDto>> getAllByPagination(Pageable pageable) throws NotFoundException {
		TPage<BookDto> data = bookServiceImp.getAllPageable(pageable);
		return ResponseEntity.ok(data);
	}

	// localhost:8182/api/book/1
	// localhost:8182/api/book/5
	@GetMapping("/{id}")
	public ResponseEntity<BookOneDto> getAll(@PathVariable(name = "id", required = true) Long id)
			throws NotFoundException {
		return ResponseEntity.ok(bookServiceImp.getOne(id));
	}

	@GetMapping("/find/{name}")
	public ResponseEntity<List<BookDto>> findByName(@PathVariable(name = "name", required = true) String name)
			throws NotFoundException {
		return ResponseEntity.ok(bookServiceImp.SearchBooksByName(name));
	}

	@PostMapping()
	public ResponseEntity<BookOneDto> createProject(@RequestBody BookOneDto bookOneDto) throws Exception {
		return ResponseEntity.ok(bookServiceImp.save(bookOneDto));
	}

	@PutMapping("/{id}")
	public ResponseEntity<BookUpdateDto> updateBook(@PathVariable(name = "id", required = true) Long id,
													@RequestBody BookUpdateDto bookUpdateDto) throws NotFoundException {
		return ResponseEntity.ok(bookServiceImp.update(id, bookUpdateDto));
	}

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteBook(@PathVariable(name = "id", required = true) Long id)
			throws NotFoundException {
		return ResponseEntity.ok(bookServiceImp.delete(id));
	}

	@GetMapping("/statuses")
	public ResponseEntity<List<BookStatus>> getAllBookStatus() {
		return ResponseEntity.ok(Arrays.asList(BookStatus.values()));
	}
}
