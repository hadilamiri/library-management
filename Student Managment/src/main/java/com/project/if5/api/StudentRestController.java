package com.project.if5.api;


import com.project.if5.model.Student;
import com.project.if5.service.imp.StudentServiceImp;
import com.project.if5.util.TPage;
import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/student")
@CrossOrigin
public class StudentRestController {


	private final StudentServiceImp studentServiceImp;

	@Autowired
	public StudentRestController(StudentServiceImp studentServiceImp) {
		super();
		this.studentServiceImp = studentServiceImp;
	}

	@GetMapping()
	public ResponseEntity<List<Student>> getAll() throws NotFoundException {
		return ResponseEntity.ok(studentServiceImp.getAll());
	}

	@GetMapping("/pagination")
	public ResponseEntity<TPage<Student>> getAllByPagination(Pageable pageable) throws NotFoundException {
		TPage<Student> data = studentServiceImp.getAllPageable(pageable);
		return ResponseEntity.ok(data);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Student> selectStudent(@PathVariable(name = "id", required = true) Long id)
			throws NotFoundException {
		return ResponseEntity.ok(studentServiceImp.findById(id));
	}

	@PostMapping()
	public ResponseEntity<Student> createStudent(@Valid @RequestBody Student student) throws Exception {
		return ResponseEntity.ok(studentServiceImp.save(student));
	}

	@PutMapping("/{id}")
	public ResponseEntity<Student> updateStudent(@PathVariable(name = "id", required = true) Long id,
			@Valid @RequestBody Student studentDto) throws Exception {
		return ResponseEntity.ok(studentServiceImp.update(id, studentDto));
	}

	@PatchMapping("/get-book")
	public ResponseEntity<Student> getBookForStudent(@Valid @RequestBody Student student)
			throws NotFoundException {
		// @Valid @RequestBody Long bookId ) throws NotFoundException {
		return ResponseEntity.ok(studentServiceImp.getBookForStudent(student));
	}

	/*@PatchMapping("/leave-book")
	public ResponseEntity<Student> leaveBookForStudent(@Valid @RequestBody Student student)
			throws NotFoundException {

		return ResponseEntity.ok(studentServiceImp.leaveBookForStudent(student);
	} */

	@DeleteMapping("/{id}")
	public ResponseEntity<Boolean> deleteBook(@PathVariable(name = "id", required = true) Long id)
			throws NotFoundException {
		return ResponseEntity.ok(studentServiceImp.delete(id));
	}
}
