package com.project.if5.service;


import com.project.if5.model.Student;
import javassist.NotFoundException;

import javax.validation.Valid;
import java.util.List;

public interface StudentService {
	public Student save(@Valid Student studentDto) throws Exception;
	//public TPage<Student> getAllPageable(Pageable pageable) throws NotFoundException;
	public List<Student> getAll() throws NotFoundException;
	public Student findById(Long id) throws NotFoundException;
	public Student update(Long id, @Valid Student studentDto) throws Exception;
	public Student getBookForStudent(@Valid Student studenPatchtDto) throws NotFoundException;
	public Boolean delete(Long id) throws NotFoundException;
	//public Student leaveBookForStudent(@Valid Student studenPatchtDto) throws NotFoundException;
}
