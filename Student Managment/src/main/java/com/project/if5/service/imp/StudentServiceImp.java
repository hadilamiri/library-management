package com.project.if5.service.imp;


import com.project.if5.model.Student;
import com.project.if5.repository.StudentRepository;
import com.project.if5.service.StudentService;
import com.project.if5.util.TPage;
import javassist.NotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@Service
public class StudentServiceImp implements StudentService {
	private final ModelMapper modelMapper;
	//private final AuthorRepository authorRepository;
	private final StudentRepository studentRepository;
	//private final BookRepository bookRepository;

	public StudentServiceImp(ModelMapper modelMapper, StudentRepository studentRepository) {
		super();
		this.modelMapper = modelMapper;
		this.studentRepository = studentRepository;
	}

	public Student save(@Valid Student student) throws Exception {
		List<Student> list = studentRepository.findByEmail(student.getEmail().trim());

		if(list.size()>0){
			throw new Exception("Student email already exist : " + student.getEmail());
		}
		list = studentRepository.findByCin(student.getCin());
		if(list.size()>0){
			throw new Exception("Student Tc no already exist : " + student.getCin());
		}
		student = modelMapper.map(student, Student.class);
		studentRepository.save(student);
		student.setId(student.getId());
		return student;
	}

	public TPage<Student> getAllPageable(Pageable pageable) throws NotFoundException {
		try {
			Page<Student> page = studentRepository.findAll(PageRequest.of(pageable.getPageNumber(),
					pageable.getPageSize(), Sort.by(Sort.Direction.ASC, "id")));
			// Page<Author> page=authorRepository.findAll(pageable);
			TPage<Student> tPage = new TPage<Student>();
			Student[] students = modelMapper.map(page.getContent(), Student[].class);

			tPage.setStat(page, Arrays.asList(students));
			return tPage;
		} catch (Exception e) {
			throw new NotFoundException("User email dosen't exist : " + e);
		}
	}

	public List<Student> getAll() throws NotFoundException {
		List<Student> students = studentRepository.findAll();
		if (students.size() < 1) {
			throw new NotFoundException("Customer don't already exist");
		}
		Student[] students1 = modelMapper.map(students, Student[].class);
		return Arrays.asList(students1);
	}

	public Student findById(Long id) throws NotFoundException {
		Optional<Student> studentOpt = studentRepository.findById(id);
		if (!studentOpt.isPresent()) {
			throw new NotFoundException("Student dosen't exist");
		}
		return modelMapper.map(studentOpt.get(), Student.class);
	}

	public Student update(Long id, @Valid Student studentDto) throws Exception {
		Optional<Student> studentOpt = studentRepository.findById(id);
		if (!studentOpt.isPresent()) {
			throw new NotFoundException("Student dosen't exist");
		}
		Student student = modelMapper.map(studentDto, Student.class);
		student.setId(id);
		studentRepository.save(student);
		studentDto.setId(student.getId());
		return studentDto;
	}

	public Student getBookForStudent(@Valid Student student) throws NotFoundException {
		Optional<Student> studentOpt = studentRepository.findById(student.getId());
		if (!studentOpt.isPresent()) {
			throw new NotFoundException("Student dosen't exist");
		}
		/*Optional<Book> bookChecked = bookRepository.findById(studenPatchtDto.getBookId());
		if (!bookChecked.isPresent()) {
			throw new NotFoundException("Book dosen't exist");
		}
		bookChecked.get().setStudent(studentOpt.get());
		bookRepository.save(bookChecked.get());*/
		return modelMapper.map(studentRepository.getOne(student.getId()), Student.class);
	}

	public Boolean delete(Long id) throws NotFoundException {
		Optional<Student> studentOpt = studentRepository.findById(id);
		if (!studentOpt.isPresent()) {
			throw new NotFoundException("Student dosen't exist");
		}
		/*if (studentOpt.get().getBooks().size() > 0) {
			studentOpt.get().getBooks().forEach(book -> {
				book.setStudent(null);
				bookRepository.save(book);
			});
		} */
		//studentOpt.get().setBooks(null);
		studentRepository.delete(studentOpt.get());
		return true;

	}

	

	/*public Student leaveBookForStudent(@Valid Student student) throws NotFoundException {
		Optional<Student> studentOpt = studentRepository.findById(student.getId());
		if (!studentOpt.isPresent()) {
			throw new IllegalArgumentException("Student dosen't exist");
		}
		Optional<Book> bookChecked = bookRepository.findById(studenPatchtDto.getBookId());
		if (!bookChecked.isPresent()) {
			throw new NotFoundException("Book dosen't exist");
		}

		bookChecked.get().setStudent(null);
		bookRepository.save(bookChecked.get());

		studentOpt.get().getBooks().remove(bookChecked.get());
		studentRepository.save(studentOpt.get());

		Student student = studentRepository.getOne(studenPatchtDto.getStudentId());
		return modelMapper.map(student, StudentDto.class);
	} */

}
