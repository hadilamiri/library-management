package com.project.if5.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "student")
public class Student {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@NotNull
	@Column(name = "cin", unique = true)
	private Long cin;

	@NotNull
	@Column(name = "username", length = 100, unique = true)
	private String fullname;

	@Email
	@Column(name = "email", length = 100, unique = true)
	private String email;

	@Column(name = "pwd", length = 300)
	private String password;

	@Column(name = "university", length = 3000)
	private String university;

	@Column(name = "department", length = 3000)
	private String department;

	@NotNull
	@Column(name = "phone", length = 100)
	private String phone;

	@Column(name = "address", length = 100)
	private String address;


	//@OneToMany(mappedBy = "student", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	//private List<Book> books;
}
