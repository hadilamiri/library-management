package com.project.if5.repository;

import com.project.if5.model.Student;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface StudentRepository extends JpaRepository<Student, Long> {

	List<Student> findByEmail(String email);
	List<Student> findByCin(@NotNull Long cin);

}
