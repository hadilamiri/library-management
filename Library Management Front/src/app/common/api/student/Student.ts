export class Student {
    public id: number;
    public fullname: string;
    public cin: string;
    public email: string;
    public phone: string;
    public university: string;
    public department: string;
    public address: string;
    constructor() { }
  }